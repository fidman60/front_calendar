import React from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {Button} from "react-native-elements";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import LinearGradient from 'react-native-linear-gradient';
import PropTypes from 'prop-types';

export default class BookingItem extends React.PureComponent {

    _handleBookingBtnPress = () => {
        console.log("réservation");
    };

    _handleBookmarkToggle = () => {
        console.log("toggle bookmark");
    };

    render() {
        const {item} = this.props;

        return (
            <View style={styles.main}>
                <TouchableOpacity onPress={this._handleBookmarkToggle} style={styles.bookmarked}>
                    <Icon
                        name="heart-outline"
                        color={item.inBookmark ? "#fc425f" : "rgb(230, 230, 230)"}
                        size={30}
                    />
                </TouchableOpacity>
                <View style={styles.imageContainer}>
                    <Image
                        source={require('../../assets/img/machine.jpg')}
                        style={styles.image}
                        resizeMode="contain"
                    />
                </View>
                <Text style={styles.title}>{item.title}</Text>
                <Text style={styles.price}>{item.price} £</Text>
                <Button
                    title="Réserver"
                    onPress={this._handleBookingBtnPress}
                    buttonStyle={styles.buttonBooking}
                    containerStyle={styles.buttonBookingContainer}
                    titleStyle={{fontSize: 13}}
                    ViewComponent={LinearGradient}
                    linearGradientProps={{
                        colors: item.isReserved ? ['#fec6cf', '#febcc3'] : ['rgb(253,78,109)', 'rgb(251,38,59)'],
                    }}
                    disabled={item.isReserved}
                    disabledTitleStyle={styles.disabledBookingTitle}
                />
            </View>
        );
    }

}

BookingItem.propTypes = {
    item: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
    main: {
        backgroundColor: "white",
        width: 150,
        padding: 10,
        borderRadius: 20,
        height: 210,
        marginRight: 10,
    },
    title: {
        color: "#484c7f",
        fontSize: 15,
        marginBottom: 5,
        height: 40
    },
    imageContainer: {
        paddingTop: 10,
        paddingBottom: 10,
    },
    image: {
        width: 80
    },
    price: {
        color: "#fc3852",
        fontWeight: "bold",
        fontSize: 20
    },
    bookmarked: {
        position: "absolute",
        right: 8,
        top: 5,
    },
    buttonBooking: {
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 0,
        borderTopRightRadius: 0,
        borderTopLeftRadius: 10,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: "#f5525f"
    },
    buttonBookingContainer: {
        position: "absolute",
        bottom: 0,
        right:0
    },
    disabledBookingTitle: {
        color: "white"
    }
});