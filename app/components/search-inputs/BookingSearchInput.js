import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Input} from "react-native-elements";
import Icon from "react-native-vector-icons/MaterialIcons";
import CustomIcon from '../icons/CustomIcon';

export default class BookingSearchInput extends React.PureComponent {

    render() {
        return (
            <Input
                leftIcon={<Icon name="search" size={25} color="#9f9fb4"/>}
                placeholder="Rechercher..."
                rightIcon={(
                    <TouchableOpacity>
                        <CustomIcon name="adjust" size={20} color="#9f9fb4"/>
                    </TouchableOpacity>
                )}
                containerStyle={styles.containerStyle}
                inputContainerStyle={styles.inputContainerStyle}
                inputStyle={styles.inputStyle}
                leftIconContainerStyle={styles.leftIconContainer}
                placeholderTextColor="#b1b1c3"
            />
        );
    }

}

const styles = StyleSheet.create({
    containerStyle: {
        backgroundColor: "white",
        borderBottomWidth: 0,
        borderRadius: 15,
        paddingLeft: 0,
    },
    inputContainerStyle: {
        borderBottomWidth: 0,
    },
    inputStyle: {
        color: "#7f7f96"
    },
    leftIconContainer: {
        marginRight: 10
    }
});