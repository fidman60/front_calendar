import React from 'react';
import {View, StyleSheet, Text, FlatList} from 'react-native';
import PropTypes from 'prop-types';
import BookingItem from "../items/BookingItem";

export default class HorizontalBookingViewList extends React.PureComponent {

    _renderItem = ({item}) => <BookingItem item={item}/>;

    _keyExtractor = (item) => item.id.toString();

    render(){
        const {title, items} = this.props;
        return (
            <View>
                <Text style={styles.title}>{title}</Text>
                <FlatList
                    data={items}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                    horizontal
                />
            </View>
        );
    }

}

HorizontalBookingViewList.propTypes = {
    title: PropTypes.string.isRequired,
    items: PropTypes.array.isRequired,
};

const styles = StyleSheet.create({
    title: {
        color: "#484c7f",
        fontSize: 18,
        fontWeight: 'bold',
        marginTop: 15,
        marginBottom: 15
    }
});

