import React from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';

export default class LoadingIndicator extends React.Component {

    shouldComponentUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): boolean {
        return false;
    }

    render() {
        return (
            <View style={styles.main_container}>
                <ActivityIndicator size={30} color="#416aa2"/>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 10,
    }
});