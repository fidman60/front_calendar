import React from "react";
import {StyleSheet, Text, View} from "react-native";
import PropTypes from 'prop-types';

export default class DayItem extends React.Component {

    shouldComponentUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): boolean {
        return false;
    }

    render() {
        const {day, inRange, isFocused} = this.props;

        const color = isFocused ? 'white' : inRange ? '#7c85a2': '#d3d6d8';
        const backgroundColor = isFocused ? '#fe6577' : 'white';
        return (
            <View style={[styles.calendarItem, {backgroundColor}]}>
                <Text style={{color}}>{day}</Text>
            </View>
        );
    }

}

DayItem.propTypes = {
    day: PropTypes.number.isRequired,
    inRange: PropTypes.bool,
    isFocused: PropTypes.bool,
};

DayItem.defaultProps = {
    isFocused: false,
    inRange: true,
};

const styles = StyleSheet.create({
    calendarItem: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "orange",
        paddingTop: 5,
        paddingBottom: 5,
        width: 35,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20

    }
});