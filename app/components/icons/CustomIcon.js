import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../../config/fonts/selection';

export default createIconSetFromIcoMoon(icoMoonConfig);