import React from 'react';
import {View, StyleSheet, Text, ScrollView} from 'react-native';
import BookingHeader from "../../../components/headers/BookingHeader";
import BookingSearchInput from "../../../components/search-inputs/BookingSearchInput";
import HorizontalBookingViewList from "../../../components/list-view/HorizontalBookingViewList";
import {bookingItems} from "../../../service/data";

export default class BookingScreen extends React.Component {

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        alert("did update !");
    }

    render() {
        return (
            <View style={styles.main_container}>
                <BookingHeader/>
                <ScrollView>
                    <View style={styles.scrollView}>
                        <View style={styles.searchInput}>
                            <BookingSearchInput/>
                        </View>
                        <HorizontalBookingViewList title="Énergie électrique et Fluides" items={bookingItems}/>
                        <HorizontalBookingViewList title="Outillage professionnel" items={bookingItems}/>
                        <HorizontalBookingViewList title="Terrassement et Routes" items={bookingItems}/>
                    </View>
                </ScrollView>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
    },
    scrollView: {
        flex: 1,
        marginTop: 20,
        paddingTop: 20,
        backgroundColor: "#e7eff8",
        paddingLeft: 20,
        borderTopRightRadius: 40,
        borderTopLeftRadius: 40,
        paddingBottom: 20
    },
    searchInput: {
        paddingRight: 20,
    }
});