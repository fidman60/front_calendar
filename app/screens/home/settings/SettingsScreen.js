import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import CustomIcon from "../../../components/icons/CustomIcon";

export default class SettingsScreen extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <Text>Settings screen</Text>
                <CustomIcon name="profile" size={20} color="red"/>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    }
});