import React from 'react';
import {Image, StyleSheet, View} from 'react-native';

export default class SplashScreen extends React.PureComponent {

    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate('App');
        }, 3000);
    }

    render() {
        return (
            <View style={styles.main_container}>
                <Image source={require('../assets/img/logo.jpg')} style={{width: 200}} resizeMode="contain"/>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    main_container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    }
});