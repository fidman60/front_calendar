import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export default class Today extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <Text>Today calender</Text>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    }
});