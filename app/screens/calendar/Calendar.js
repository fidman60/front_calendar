import React from 'react';
import {Animated, Dimensions, Easing, ScrollView, StyleSheet, View} from 'react-native';
import Icon from "react-native-vector-icons/MaterialIcons";
import EventsList from "../../components/calendar/EventsList";
import {events} from "../../service/data";
import DateRangePicker from "../../components/calendar/DateRangePicker";
import DatePickerPlaceHolder from "../../components/calendar/DatePickerPlaceHolder";
import {CALENDAR_HEIGHT, calendarStyle, calendarTheme} from "../../config/calendarTheme";
import moment from 'moment';
import {Button} from "react-native-elements";
import {capitalize} from "../../helpers/functions";
import LoadingIndicator from "../../components/loading/LoadingIndicator";
import CalendarHeader from "../../components/headers/CalendarHeader";

const {width} = Dimensions.get("window");

const INITIAL_RANGE = ['2019-10-14', '2019-10-20'];

export default class Calendar extends React.Component {

    constructor(props) {
        super(props);

        this._calendarAnim = new Animated.Value(0);

        this.state = {
            showCalendar: false,
            events: [],
            dates: [],
            loading: false,
            month: 'Aucun'
        };

        this.dates = [];

        this.firstUpdate = true;

        this._calendarOpacity = this._calendarAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
        });

        this._calendarYTranslate = this._calendarAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [-CALENDAR_HEIGHT/.22, 0],
        });

        this._calendarPlaceHolder = this._calendarAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0],
        });

        this._calendarYTranslatePlaceHolder = this._calendarAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [0, -100],
        });
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        // initial list
        if (this.firstUpdate && this.state.dates.length > 0) {
            this.firstUpdate = false;
            this._onSearch();
        }

        if (this.state.showCalendar !== prevState.showCalendar) {
            this._animate();
        }
    }

    _animate(){
        Animated.timing(this._calendarAnim, {
            toValue: this.state.showCalendar ? 1 : 0,
            duration: 300,
            ease: Easing.ease,
            useNativeDriver: true,
        }).start();
    }

    _handleCalendarToggle = () => {
        this.setState({
            showCalendar: !this.state.showCalendar
        });
    };

    _onSelectRange = (dates) => {
        this.setState({
            dates: dates,
        });
    };

    _onSearch = () => {
        const {dates} = this.state;

        this.setState({
            loading: true,
        });

        setTimeout(() => {
            const filteredEvents = events.filter((item) => {
                for (let i = 0;i < dates.length;i++) {
                    if (moment(item.date).isSame(dates[i])) return true;
                }
                return false;
            });

            this.setState({
                events: filteredEvents,
                loading: false,
                dates: [],
                showCalendar: filteredEvents.length <= 0,
                month: filteredEvents.length > 0 ? capitalize(moment(filteredEvents[0].date).format("MMMM")) : "Aucun",
            });
        }, 2000);
    };

    render() {
        const {showCalendar, events, dates, loading, month} = this.state;

        return (
            <View style={styles.container}>
                <CalendarHeader month={month} handleCalendarToggle={this._handleCalendarToggle}/>
                <View style={styles.secondSection}>
                    <Animated.View style={[styles.calendarBlock, {opacity: this._calendarOpacity,transform: [{translateY: this._calendarYTranslate}]}]} >
                        <DateRangePicker
                            style={calendarStyle}
                            initialRange={INITIAL_RANGE}
                            onSuccess={this._onSelectRange}
                            theme={calendarTheme}
                        />
                        <Button
                            icon={<Icon name="search" size={20} color="white" style={styles.searchBtnIcon}/>}
                            title="Appliquer"
                            buttonStyle={styles.searchBtn}
                            loading={loading}
                            onPress={this._onSearch}
                            disabled={dates.length < 1}
                        />
                    </Animated.View>
                    <Animated.View style={[styles.calendarBlock, {opacity: this._calendarPlaceHolder, transform: [{translateY: this._calendarYTranslatePlaceHolder}]}]} >
                        <DatePickerPlaceHolder/>
                    </Animated.View>
                    <ScrollView style={[styles.scrollView, {marginTop: showCalendar ? 330 : 35,}]}>
                        <View style={styles.scrollViewChild}>
                            {events.length > 0 && !loading && <EventsList events={events}/>}
                            {loading && !showCalendar && <LoadingIndicator/>}
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#e1eefe"
    },
    calendarBlock: {
        position: "absolute",
        width: width - 20, //padding left and right width
        top: -35,
        zIndex: 100,
        backgroundColor: "white",
        shadowColor: "white",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.1,
        shadowRadius: 1.65,
        elevation: 6,
        borderRadius: 5,
    },
    secondSection: {
        flex: 1,
        paddingTop: 0,
        alignItems: "center"
    },
    searchBtn: {
        borderRadius: 0,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
    },
    scrollView: {
        width: "100%",
        position: "relative",
        flex: 1,
    },
    alertContainer: {
        width: "100%",
        marginTop: 10,
        paddingRight: 10,
        paddingLeft: 10
    },
    scrollViewChild: {
        flex: 1,
        paddingBottom: 10
    },
    searchBtnIcon: {
        marginRight: 5
    }
});

